# Haskell: From Beginner to Intermediate #11

## Advent of Code 2018 - Day 1

### FP AMS 21/04/2021 

---

![right 100%](learning-curve.png)

# Overall Planning

1. Basics
2. Functors, Applicative Functors, and Monoids
3. Monads
4. Lenses, Folds, and Traversals
5. Monad Transformers
6. Prisms and Isos
7. Type-Level Programming

---

![inline 50%](book-cover.jpg)![inline 64%](book-of-monads.png)

---

# Advent of Code 2018 - Day 1

```haskell
{-# LANGUAGE ScopedTypeVariables #-}

module Exercise1 (module Exercise1) where

import Data.Set (Set)
import qualified Data.Set as Set
import Paths_homework

main :: IO ()
main = do
  file <- getDataFileName "data/1a.txt"
  input <- readFile file
  print $ sum (deltas input)
  print $ findRepeat 0 (Set.fromList [0]) (cycle (deltas input))

deltas :: String -> [Integer]
deltas = fmap read . lines . filter (/= '+')

findRepeat :: Integer -> Set Integer -> [Integer] -> Integer
findRepeat x xs (d : ds) =
  let x' = x + d
   in if Set.member x' xs
        then x'
        else findRepeat x' (Set.insert x' xs) ds

```

---

# Planning

Starting from May 5th:

- Alternating Dutch and English session
- Build a JSON library
- Watch Slack and meetup.com for details
