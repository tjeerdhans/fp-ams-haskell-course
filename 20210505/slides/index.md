# Haskell: From Beginner to Intermediate #12

## Writing a JSON Library - Part 1

### FP AMS 12/05/2021

---

![right 100%](learning-curve.png)

# Overall Planning

1. Basics
2. Functors, Applicative Functors, and Monoids
3. Monads
4. Lenses, Folds, and Traversals
5. Monad Transformers
6. Prisms and Isos
7. Type-Level Programming

---

![inline 40%](book-cover.jpg)![inline 40 %](real-world-haskell.jpg)![inline 50%](book-of-monads.png)

---

# Writing a JSON Library

## Planning

1. Introduction
1. Representing JSON data:
   1. data type
   1. accesor functions
1. Output: pretty printing JSON data
1. Input: parsing JSON data
1. Aeson

---

# [fit] BORING?
# [fit] Yes, but …

---

# Aeson - Sneak Peek (1/2)

```json
{ 
  "first-name": "Michel",
  "last-name": "Rijnders"
  "address": {
    "street": "Kerkstraat",
    "number": 42,
    "city": "Amsterdam"
  }
}
```

---

# Aeson - Sneak Peek (2/2)

```haskell
{-# LANGUAGE DeriveGeneric, DeriveAnyClass #-}

import GHC.Generics
import Data.Aeson
import Text.Casing (kebab)

data Person = Person
  { firstName :: String,
    lastName :: String,
    address :: Address
  }
  deriving (Generic)

instance FromJSON Person where
  parseJSON = genericParseJSON defaultOptions {fieldLabelModifier = kebab}
  
data Address = Address
  { street :: String,
    number :: Int,
    city : String
  }
  deriving (Generic, FromJSON)
```

---

# Introduction (1/3)

- _J_ava_S_cript _O_bject _N_otation
- lightweight data-interchang format
- based on a subset of JavaScript
- [json.org](https://www.json.org/json-en.html)

---

# Introduction (2/3)

## Simple Types

- strings
- numbers
- booleans
- `null`

---

# Introduction (3/3)

## Compound Types

- array
- object

---

# Representing JSON Data (1/3)

```haskell
data JValue = JString String
            | JNumber Double
            | JBool Bool
            | JNull
            | JObject [(String, JValue)]
            | JArray [JValue]
              deriving (Eq, Ord, Show)
```

---

# Representing JSON Data (2/3)

```haskell
getString :: JValue -> Maybe String
getString (JString s) = Just s
getString _           = Nothing              
```

---

# Representing JSON Data (3/3)

```haskell
getInt :: JValue -> Maybe Integer

getDouble :: JValue -> Maybe Double

getBool :: JValue -> Maybe Bool

isNull :: JValue -> getBool

getObject :: JValue -> Maybe [(String, JValue)]

getArray :: JValue -> Maybe [JValue]
```

---

# Pretty Printing JSON Data (1/n)

```haskell
module PutJSON where

import Data.List (intercalate)
import SimpleJSON

renderJValue :: JValue -> String

renderJValue (JString s) = show s
renderJValue (JNumber n) = show n
renderJValue (JBool True) = "true"
renderJValue (JBool False) = "false"
renderJValue JNull = "null"
```

---
  
# Pretty Printing JSON Data (2/n)

```haskell
renderJValue (JObject o) = "{" ++ pairs o ++ "}"
  where 
    pairs [] = ""
    pairs ps = intercalate ", " (map renderPair ps)
    renderPair (k,v)   = show k ++ ": " ++ renderJValue v

renderJValue (JArray a) = "[" ++ values a ++ "]"
  where 
    values [] = ""
    values vs = intercalate ", " (map renderJValue vs)
```

---

# Pretty Printing JSON Data (3/n)

```haskell
putJValue :: JValue -> IO ()
putJValue v = putStrLn (renderJValue v)
```
