import Data.Maybe (mapMaybe)
import Text.Read  (readMaybe)
import Prelude

main :: IO ()
main = do
  print $ isValid "4012888888881881" -- OK
  print $ isValid "4012888888881882" -- Not Ok

-- Helper Functions
double :: Int -> Int
double x = x + x

creditSum :: [Int] -> Int
creditSum [] = 0
creditSum (x : xs)
  | x == 10 = 1 + creditSum xs
  | x < 10 = x + creditSum xs
  | x > 10 = (x - 10 + 1) + creditSum xs

applyEvens :: (a -> a) -> [a] -> [a]
applyEvens _ [] = []
applyEvens _ [x] = [x]
applyEvens fn (y : x : xs) = y : fn x : applyEvens fn xs

splitChars :: [a] -> [[a]]
splitChars [] = []
splitChars [x] = [[x]]
splitChars (x : xs) = [x] : splitChars xs

-- Main Functions
readToInts :: String -> Either String [Int]
readToInts input
  | length nums == length input = Right nums
  | otherwise = Left "Found characters"
  where
    nums = mapMaybe (readMaybe :: String -> Maybe Int) $ splitChars input

splitToGroups :: [Int] -> Either String [Int]
splitToGroups input
  | length input > 16 = Left "Too many digits"
  | length input < 7 = Left "Too little digits"
  | otherwise = Right input
  where
    inputLength = length input

checkSum :: [Int] -> Either String [Int]
checkSum input
  | total == 0 = Right input
  | otherwise = Left "Checksum failed"
  where
    total = (creditSum . applyEvens double . reverse) input `mod` 10

showToString :: [Int] -> String
showToString = concatMap show

isValid :: [Char] -> Either String String 
isValid xs = showToString <$> (readToInts xs >>= splitToGroups >>= checkSum)
