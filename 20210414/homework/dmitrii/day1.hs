-- "Error: Device must be calibrated before first use. Frequency drift detected. Cannot maintain destination lock."

{- Exercise 1:
    Part 1:
      Given input (in day1_input.txt),
      Starting with a frequency of zero, what
      is the resulting frequency after all of
      the changes in frequency have been applied?
    Part 2:
      To calibrate the device, you need to find
      the first frequency it reaches twice.
      NB: Note that your device might need to
      repeat its list of frequency changes many
      times before a duplicate frequency is found,
      and that duplicates might be found while in
      the middle of processing the list.
 -}

module DmitriiOrlovDay1 where

import Data.Maybe
import Data.Set
import System.IO (IOMode (ReadMode), hGetContents, openFile)
import Text.Read

main :: IO ()
main = do
  handle <- openFile "day1_input.txt" ReadMode
  contents <- hGetContents handle
  let numbers = mapMaybe readAdjustment
  -- Part 1:
  print $ sum . numbers . lines $ contents
  -- Part 2:
  print $ getRepeatedFrequency . cycle . numbers . lines $ contents

-- Part 1: utilities

readAdjustment :: String -> Maybe Integer
readAdjustment ('+' : rest) = readMaybe rest
readAdjustment ('-' : rest) = (* (-1)) <$> readMaybe rest
readAdjustment otherString = Nothing

-- Part 2: utilities

getRepeatedFrequency :: [Integer] -> Integer
getRepeatedFrequency = getRepeatedSumValue 0 (fromList [0])

getRepeatedSumValue :: Integer -> Set Integer -> [Integer] -> Integer
getRepeatedSumValue a s (x : xs) =
  if member a' s
    then a'
    else getRepeatedSumValue a' s' xs
  where
    a' = a + x
    s' = insert a' s
