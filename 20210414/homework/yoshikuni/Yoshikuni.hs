{-# LANGUAGE ScopedTypeVariables #-}

module Yoshikuni where

import Paths_homework
-- import Text.Read

-- https://adventofcode.com/2018/day/1


readInteger :: [Char] -> Integer
readInteger ('-':xs) = (read xs :: Integer) * (-1)
readInteger ('+':xs) = read xs :: Integer
readInteger string = read string :: Integer

main :: IO ()
main = do
  file <- getDataFileName "data/1a.txt"
  input <- readFile file

  -- let strings = lines input
  -- let integers :: [Integer] = fmap readInteger strings
  -- let result :: Integer = foldl (+) 0 integers

  let result :: Integer = (sum . fmap readInteger . lines) input

  print result
  pure ()

