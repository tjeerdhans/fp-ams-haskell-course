{-# LANGUAGE ScopedTypeVariables #-}

module Exercise1 (module Exercise1) where

import Data.Set (Set)
import qualified Data.Set as Set
import Paths_homework

-- https://adventofcode.com/2018/day/1

main :: IO ()
main = do
  file <- getDataFileName "data/1a.txt"
  input <- readFile file
  print $ sum (deltas input)
  print $ findRepeat 0 (Set.fromList [0]) (cycle (deltas input))

-- | The 'deltas' function converts the input to a list of deltas/frequency changes
deltas :: String -> [Integer]
deltas = fmap read . lines . filter (/= '+')

-- | The 'findRepeat' function finds the 1st repeating frequency
findRepeat ::
  -- | The current frequency
  Integer ->
  -- | The set of frequencies already seen
  Set Integer ->
  -- | The input/deltas
  [Integer] ->
  -- | The result, i.e. the 1st repeating frequency
  Integer
findRepeat x xs (d : ds) =
  let x' = x + d
   in if Set.member x' xs
        then x'
        else findRepeat x' (Set.insert x' xs) ds
