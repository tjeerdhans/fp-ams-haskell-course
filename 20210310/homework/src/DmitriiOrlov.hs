{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeSynonymInstances #-}

module DmitriiOrlov where

import qualified Data.Map as M
import ExprT
import Parser
import StackVM

{- Exerise 1:

    Evaluate the expressions of ExprT.
 -}

eval :: ExprT -> Integer
eval expr = case expr of
  ExprT.Lit n -> n
  -- ExprT.Add expr1 expr2 -> (sum . map eval) [expr1, expr2]
  -- but better readable as
  ExprT.Add expr1 expr2 -> eval expr1 + eval expr2
  -- ExprT.Mul expr1 expr2 -> (product . map eval) [expr1, expr2]
  -- but better readable as
  ExprT.Mul expr1 expr2 -> eval expr1 * eval expr2

{- Exercise 2:

    Using the implemented Parser.hs, evaluate strings
 -}

maybeEval :: Maybe ExprT.ExprT -> Maybe Integer
maybeEval expr = case expr of
  Just e -> Just (eval e)
  Nothing -> Nothing

evalStr :: String -> Maybe Integer
evalStr = maybeEval . Parser.parseExp ExprT.Lit ExprT.Add ExprT.Mul

{- Exercise 3:

    You decide to abstract away the properties of ExprT with a type class.
    Create a type class called Expr with three methods called lit, add,
        and mul which parallel the constructors of ExprT.
 -}

class Expr a where
  lit :: Integer -> a
  add :: a -> a -> a
  mul :: a -> a -> a

instance Expr ExprT where
  lit = ExprT.Lit
  add = ExprT.Add
  mul = ExprT.Mul

--   remark
reify :: ExprT -> ExprT
reify = id

{- Exercise 4:

    Make new instances of Expr for Integer, Bool, MinMax, Mod7
 -}

instance Expr Integer where
  lit = id
  add = (+)
  mul = (*)

instance Expr Bool where
  lit n = n > 0
  add = (||)
  mul = (&&)

newtype MinMax = MinMax Integer deriving (Eq, Show)

instance Expr MinMax where
  lit = MinMax
  add (MinMax x) (MinMax y) = (lit . max x) y
  mul (MinMax x) (MinMax y) = (lit . min x) y

newtype Mod7 = Mod7 Integer deriving (Eq, Show)

instance Expr Mod7 where
  lit n = Mod7 (mod n 7)
  add (Mod7 x) (Mod7 y) = lit $ x + y
  mul (Mod7 x) (Mod7 y) = lit $ x * y

-- testing
testExp :: Expr a => Maybe a
testExp = parseExp lit add mul "(3 * -4) + 5"

testInteger = testExp :: Maybe Integer

testBool = testExp :: Maybe Bool

testMM = testExp :: Maybe MinMax

testSat = testExp :: Maybe Mod7

{- Exercise 5:

    Implement the translation to CPU instructions.
    Create an instance of the Expr type class for Program.
    Create a function to compile: compile :: String -> Maybe Program
 -}

newtype TProgram = TProgram {unwrap :: StackVM.Program}

instance Expr TProgram where
  lit n = TProgram [StackVM.PushI n]
  add n1 n2 = TProgram (unwrap n1 ++ unwrap n2 ++ [StackVM.Add])
  mul n1 n2 = TProgram (unwrap n1 ++ unwrap n2 ++ [StackVM.Mul])

compile :: String -> Maybe Program
compile = parseExp unwrapLit unwrapAdd unwrapMul
  where
    unwrapLit = unwrap . lit
    unwrapAdd n1 n2 = unwrap (add (TProgram n1) (TProgram n2))
    unwrapMul n1 n2 = unwrap (mul (TProgram n1) (TProgram n2))

{- Exercise 6:

  Give arithmetic expressions the ability to contain variables.
 -}

class HasVars a where
  var :: String -> a

data VarExprT
  = Var String
  | VLit Integer
  | VAdd VarExprT VarExprT
  | VMul VarExprT VarExprT
  deriving (Show, Eq)

instance HasVars VarExprT where
  var = Var

instance Expr VarExprT where
  lit = VLit
  add = VAdd
  mul = VMul

instance HasVars (M.Map String Integer -> Maybe Integer) where
  var v = M.lookup v

maybeCalc :: (a -> b -> c) -> Maybe a -> Maybe b -> Maybe c
maybeCalc _ Nothing _ = Nothing
maybeCalc _ _ Nothing = Nothing
maybeCalc calcF (Just x) (Just y) = (Just . calcF x) y

instance Expr (M.Map String Integer -> Maybe Integer) where
  lit n = \m -> Just n
  add fx fy = \m -> maybeCalc (+) (fx m) (fy m)
  mul fx fy = \m -> maybeCalc (*) (fx m) (fy m)

withVars ::
  [(String, Integer)] ->
  (M.Map String Integer -> Maybe Integer) ->
  Maybe Integer
withVars vs exp = exp $ M.fromList vs

{- -- `withVars` Test Cases:
--    (Uncommmend or copy to GHCI to test;
--   run `withVarsTestSuite`;
--  expect a True in the output)

testWithVars1 = withVars [("x", 6)] $ add (lit 3) (var "x")
testWithVars2 = withVars [("x", 6)] $ add (lit 3) (var "y")
testWithVars3 = withVars [("x", 6), ("y", 3)] $ mul (var "x") (add (var "y") (var "x"))
-- Uncomment only if copying into this module
-- checkTests :: [(Maybe Integer, Maybe Integer)] -> Bool
checkTests = foldl (\a tx -> a && uncurry (==) tx) True
withVarsTestSuitePassed = checkTests [(testWithVars1, Just 9), (testWithVars2, Nothing), (testWithVars3, Just 54)]
-- -}