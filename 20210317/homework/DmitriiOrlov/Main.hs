{- Come up with an instance of Functor ((->) r) -}

-- Considering the type ((->) r), it is a type constructor giving corresponding
--  to functions of signature (r -> a) where `a` is a unbound "any" type.
-- Then, given type ((->) r), we can derive the type of fmap for this instance
--  fmap :: (a -> b) -> (((->) r) a) -> ((->) r) b
-- Equal, if we execute the applications, to
--  fmap :: (a -> b) -> (r -> a) -> r -> b
-- If we look at the type signatures, we can notice
--  that (r -> a) composed with (a -> b) would have the type (r -> b).
-- Which suggests that (.) is the fmap equivalent for this Functor's instance

-- Noticing that declaring an instance of
-- `Functor ((->) r) duplicates one in `GHC.Base`
class Functor' f where
  fmap' :: (a -> b) -> f a -> f b

instance Functor' ((->) r) where
  fmap' = (.)

-- To test our implementation, we can construct an fmap' with
-- first parameter, a function `g :: (a -> b)`
--  say `a` = `Integer` and `b` = `Bool`.
-- Then our fmap' should be such that it will apply `g` to every value
--  of the co-domain of its next argument.
-- Which exactly is the definion of composition (.).

testFmap' = fmap' (> 30) (* 7)

testComp' = (> 30) . (* 7)

testCorrectness :: [Integer] -> Bool
testCorrectness inputs = foldl checkEqual True resultTuples
  where
    checkEqual = \a v -> a && uncurry (==) v
    resultTuples = zip (map testFmap' inputs) (map testFmap' inputs)