module Lib where

data Tree a
  = Leaf
  | Node a (Tree a) (Tree a)
  deriving (Show)

instance Functor Tree where
  fmap f Leaf = Leaf
  fmap f (Node x l r) = Node (f x) (fmap f l) (fmap f r)
