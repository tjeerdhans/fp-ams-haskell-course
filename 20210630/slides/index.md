# Haskell: From Beginner to Intermediate #16

## Monads Part 1: Discovering Monads

### FP AMS 30/06/2021

---

![right 100%](learning-curve.png)

# Overall Planning

1. Basics
2. Functors, Applicative Functors, and Monoids
3. **Monads**
4. Lenses, Folds, and Traversals
5. Monad Transformers
6. Prisms and Isos
7. Type-Level Programming

---

![inline](book-of-monads.png)

---
 
# Recap

Examples:

- state contexts
- lists
- `Maybe`

Perspectives:

- monads as contexts
- monads as containers

---

# State Contexts

## Binary Tree

```haskell
data Tree a
  = Leaf a
  | Node (Tree a) (Tree a)
```

## Counting the Number of Leaves

```haskell
numberOfLeaves :: Tree a -> Integer
numberOfLeaves (Leaf _) = 1
numberOfLeaves (Node l r) = numberOfLeaves l + numberOfLeaves r
```

---

# State Contexts

## Relabeling the Leaves

> If we start with a tree `t`, the result of `relabel` should contain the same elements, but with each one paired with the index it would receive if the leaves of `t` were flattened into a list starting with the leftmost leaf and ending with the rightmost leave.

```haskell
> relabel $
    Node (Leaf 'a')
         (Node (Leaf 'b')
               (Leaf 'c'))

Node (Leaf (1, 'a'))
     (Node (Leaf (2, 'b'))
           (Leaf (3, 'c')))
```

---

# State Contexts

## Relabeling the Leaves

```haskell
relabel :: Tree a -> Tree (Int, a)
relabel (Leaf x) = Leaf (???, x)
relabel (Node l r) = Node (relabel l) (relabel r)
```

```haskell
relabel :: Tree a -> Int -> (Tree (Int, a), Int)
relabel (Leaf x) i = (Leaf (i, x), i+1)
relabel (Node l r) i =
  let (l', i1) = relabel l i
      (r', i2) = relabel r i1
   in (Node l' r', i2)
```

---

# State Contexts

## Relabeling the Leaves

```haskell
type WithCounter a = Int -> (a, Int)

relabel :: Tree a -> WithCounter (Tree a)

relabel (Leaf x) i = (Leaf (i, x), i+1)
relabel (Node l r) i =
  let (l', i1) = relabel l i
      (r', i2) = relabel r i1
   in (Node l' r', i2)
```

---

# State Contexts

## Relabeling the Leaves

```haskell
type WithCounter a = Int -> (a, Int)

relabel (Node l r) i =
  let (l', i1) = relabel l i
      (r', i2) = relabel r i1
   in (Node l' r', i2)
   
let (x, newCounter) = … oldCounter
 in nextAction … x … newCounter
 
next :: WithCounter a -> (a -> WithCounter b) -> WithCounter b
f `next` g = \i -> let (x, i') = f i in g x i'

pure :: a -> WithCounter a
pure x = \i -> (x, i) 
```

---

# State Contexts

## Relabeling the Leaves

```haskell
type WithCounter a = Int -> (a, Int)

next :: WithCounter a -> (a -> WithCounter b) -> WithCounter b
f `next` g = \i -> let (x, i') = f i in g r i'

pure :: a -> WithCounter a
pure x = \i -> (x, i) 

relabel ( Leaf x) = \i -> (Leaf (i,x), i + 1)
relabel ( Node l r) = relabel l `next` \l' -> 
                      relabel r `next` \r' -> 
                      pure (Node l' r') 
```

---

# State Contexts

```haskell
type WithCounter a = Int -> (a, Int)

type State s a = s -> (s,a)
```

---

# Lists
 
```haskell
map :: (a -> b) -> [a] -> [b] 

singleton :: a -> [a]

concat :: [[a]] -> [a]
```

---

# `Maybe`

```haskell
data Maybe a = Nothing
             | Just a
             
type Name = String 
data Person = Person {name :: Name, age :: Int} 

validateName :: String -> Maybe Name 
validateAge  :: String -> Maybe Int 
```

---

# `Maybe`

```haskell
validatePerson :: String -> Int -> Maybe Person 
validatePerson name age = 
  case validateName name of 
    Nothing    -> Nothing 
    Just name' -> case validateAge age of 
      Nothing   -> Nothing 
      Just age' -> Just (Person name' age') 

case v of 
  Nothing -> Nothing 
  Just v' -> nextAction … v' …
```

---

# `Maybe`

```haskell
then_ :: Maybe a -> (a -> Maybe b) -> Maybe b
then_ v g = case v of 
              Nothing -> Nothing 
              Just v' -> g v' 
              
validatePerson name age = 
  validateName name `then_` \name' -> 
  validateAge  age  `then_` \age'  -> 
  Just (Person name' age') 
  
then_ :: Maybe a   -> (a -> Maybe b)   -> Maybe b 
next  :: State s a -> (a -> State s b) -> State s b
```
---

# `Maybe`

```haskell
map :: (a -> b) -> Maybe a -> Maybe b
map f Nothing  = Nothing 
map f (Just x) = Just (f x) 

singleton :: a -> Maybe a
singleton = Just 

flatten :: Maybe (Maybe a) -> Maybe a
flatten (Just (Just x)) = Just x
flatten _               = Nothing 
```

---

# Contexts and Containers

Can we define `flatten` using `then_`?

```haskell
then_   :: Maybe a         -> (a -> Maybe b) -> Maybe b 
flatten :: Maybe (Maybe c)                   -> Maybe c
```

To make the types match:

- make `a` equal to `Maybe c`
- make `b` equal to `c` 

```haskell
then_ :: Maybe (Maybe c) -> (Maybe c -> Maybe c) -> Maybe c 
```

---

# Contexts and Containers

To make the types match:

- make `a` equal to `Maybe c`
- make `b` equal to `c` 

```haskell
then_ :: Maybe (Maybe c) -> (Maybe c -> Maybe c) -> Maybe c 
```

We need to provide `then_` with a function of type `Maybe c -> Maybe c`:

```haskell
flatten xx = xx `then_` id
```

---

# Contexts and Containers

Can we go in the opposite direction? Can we define `then_` using `flatten`?

```haskell
then_    :: Maybe a -> (a -> Maybe b) -> Maybe b
flip map :: Maybe c -> (c ->       d) -> Maybe d 
```

Try to make the types match:

- make `c` equal to `a`
- make `d` equal to `Maybe b`

```haskell
flip map :: Maybe a -> (a -> Maybe b) -> Maybe (Maybe b)
```

---

# Contexts and Containers

Try to make the types match:

- make `c` equal to `a`
- make `d` equal to `Maybe b`

```haskell
flip map :: Maybe a -> (a -> Maybe b) -> Maybe (Maybe b)
```

`flatten` to the rescue:

```haskell
then_ o f = flatten (fmap f o) 
```

---

# Monads

## Monad as a Context

```haskell
class Monad m where 
  return :: a -> m a
  (>>=)  :: m a -> (a -> m b) -> m b 
```

## Monad as a Container

```haskell
class Monad m where 
  return :: a -> m a
  fmap   :: (a -> b) -> m a -> m b
  join   :: m (m a) -> m a 
```
