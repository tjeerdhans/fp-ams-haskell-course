# Haskell: From Beginner to Intermediate #8

## Applicative Functors

### FP AMS 31/03/2021 

---

# Goal

![inline](learning-curve.png)

---

# Overall Planning

1. Basics
2. Functors, **Applicative Functors**, and Monoids
3. Monads
4. Lenses, Folds, and Traversals
5. Monad Transformers
6. Prisms and Isos
7. Type-Level Programming

---

![inline 150%](typeclasses.svg)

source: [Typeclassopedia](https://wiki.haskell.org/Typeclassopedia)

---

![inline 50%](book-cover.jpg)

---

![inline 60%](book-of-monads.png)

---

# Last Week:

## [fit] Functor
## [fit] Laws
---

# Last Week

## Functor Laws

### 1. Identity

```
fmap id == id
```

### 2. Composition

```
fmap (f . g) == fmap f . fmap g
```
  
---

# This Week:
# [fit] Applicative
# [fit] Functors

---

# Remember ...

## Currying?

```
a -> b -> c == a -> (b -> c)
```

---

# Remember ...

## Application?

```
($) :: (a -> b) -> a -> b
```

---

# Remember ...

## The `Functor` Class?


```haskell
class Functor f where
  fmap :: (a -> b) -> f a -> f b
```

---

# Remember ...

## The Inline Version of `fmap`?

```
(<$>) :: Functor f => (a -> b) -> f a -> f b
```

---

# Note ...

## How Much `$` And `<$>` Are Alike?

```
 ($)  ::              (a -> b) ->   a ->   b
(<$>) :: Functor f => (a -> b) -> f a -> f b
```

---

![inline](hypnosis.png)

---

# Applicative Typeclass

```haskell
class Functor f => Applicative f where
  pure :: a -> f a
  (<*>) :: f (a -> b) -> f a -> f b 
```

Note: we're ignoring `liftA2` for now

---

# `Maybe` Instance

```haskell
instance Applicative Maybe where
  pure = Just
  Nothing  <*> _ = Nothing
  (Just f) <*> x = fmap f x
```

---

# `[]` Instance

```haskell
instance Applicative [] where
  pure x = [x]
  fs <*> xs = [f x | f <- fs, x <- xs]
```

---

# Homework

- [https://www.cis.upenn.edu/~cis194/spring13/hw/10-applicative.pdf](https://www.cis.upenn.edu/~cis194/spring13/hw/10-applicative.pdf)
- [https://www.cis.upenn.edu/~cis194/spring13/extras/10-applicative/AParser.hs](https://www.cis.upenn.edu/~cis194/spring13/extras/10-applicative/AParser.hs)
