# Haskell: From Beginner to Intermediate #17

## Summer Break Review

### FP AMS 14/07/2021

---

![right 100%](learning-curve.png)

# Overall Planning

1. Basics
2. Functors, Applicative Functors, and Monoids
3. **Monads**
4. Lenses, Folds, and Traversals
5. Monad Transformers
6. Prisms and Isos
7. Type-Level Programming

---

![inline](book-of-monads.png)

---

# Typeclasses

- Semigroup
- Monoid
- Functor
- Applicative
- Monad

---

# Semigroup

Types with an associative binary operation:

^ Types that can "build" on themselves by appending

```haskell
class Semigroup a where 
  -- mappend
  (<>) :: a -> a -> a
 ```
 
Example:

```
λ> [1,2,3] <> [4,5,6]
[1,2,3,4,5,6]
```

Associativity:

```haskell
x <> (y <> z) = (x <> y) <> z
```

---

# Monoid

Types with an associative binary operation that have an identity element:

```haskell
class Semigroup a => Monoid a where
  mempty :: a
```

Example:

```
λ> "Aloha" <> mempty
"Aloha"
```

Right and left identity:

```
x <> mempty = x
mempty <> x = x
```

--- 

# Functor

^
- A container of elements
- Can apply a *transformation* on those elements
- Transformation *preserves* the internal structure

```haskell
class Functor f where
  fmap :: (a -> b) -> f a -> f b 
  (<$>) = fmap
```

Identity:

```haskell
fmap id == id
```

Composition

```haskell
fmap (f . g) == fmap f . fmap g
```

---

# Functor

Some instances:

```haskell
instance Functor [] where 
  fmap = map

instance Functor Maybe where 
  fmap _ Nothing  = Nothing 
  fmap f (Just a) = Just (f a)
  
instance Functor Either where 
  fmap _ (Left a)  = Left a
  fmap f (Right b) = Right (f b)
```

---

# Applicative Functor

A functor with application, providing operations to

- embed pure expressions (`pure`), and
- sequence computations and combine their results (`<*>`).

```haskell
class Functor f => Applicative f where
  pure :: a -> f a
  -- apply over
  (<*>) :: f (a -> b) -> f a -> f b
```

---

# Using `fmap`

```haskell
λ> (+) <$> Just 1 <*> Just 2
Just 3
λ> :t (fmap (+))
(fmap (+)) :: (Functor f, Num a) => f a -> f (a -> a)
```

---

# Applicative Functor

Some instances:

```haskell
instance Applicative Maybe where 
  pure = Just
  Nothing <*> _ = Nothing
  _ <*> Nothing = Nothing
  Just f <*> Just a = Just (f a)
  
instance Applicative [] where
  pure a = [a]
  fs <*> as = [f a | f <- fs, a <- as]
```

---

# Monad

An abstract datatype of actions:

^
- Context in which a computation takes place
- Specifies how to combine operations

```haskell
class Applicative m => Monad m where 
  return :: a -> m a
  -- bind
  (>>=) :: m a -> (a -> m b) -> m b
```

---

# Comparing Functions

```
λ> :t (flip (>>=))
(flip (>>=)) :: Monad m => (a -> m b) -> m a -> m b
λ> :t (=<<)
(=<<) :: Monad m => (a -> m b) -> m a -> m b
```

```haskell
(<$>) :: (a -> b)   -> f a -> f b 
(<*>) :: f (a -> b) -> f a -> f b 
(=<<) :: (a -> f b) -> f a -> f b
```

---

# Maybe Monad

Computations that might fail

```haskell
instance Monad Maybe where 
  return = Just
  Nothing  >>= _ = Nothing 
  (Just a) >>= f = Just (f a)
```
---

# List Monad

Non-deterministic computations

```haskell
instance Monad [] where
  return a = [a]
  xs >>= f = [y | x <- xs, y <- f x]
```
