module RolandHelpers where

-- This will probably be wrong for very small fractions
isInt :: Double -> Bool
isInt x = x == fromInteger (round x)

sequencePair :: (a, Either b c) -> Either b (a, c)
sequencePair (x, Right y) = Right (x, y)
sequencePair (x, Left y) = Left y
