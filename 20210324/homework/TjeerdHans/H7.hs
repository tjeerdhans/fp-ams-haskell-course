module Homework7 where


import Prelude hiding (Either, Left, Right)

 -- Homework day 7

-------------------------------------------------------------------------------------
-- exercise 1 - homework from the slides

-- prove that `Either a` and `[]` abide to the functor laws:

-- fmap id = id
-- fmap (f . g) = fmap f . fmap g

-------------------------------------------------------------------------------------
-- First, retrying the proofs for Maybe, because the slides haven't been committed yet.

--fmap :: (a -> b) -> Maybe a -> Maybe b
-- instance Functor Maybe where
--   fmap f Nothing  = Nothing
--   fmap f (Just x) = Just (f x)

-- 1st law: fmap id = id

-- fmap id Nothing == id Nothing == Nothing
-- fmap id (Just x) == Just (id x) == Just x

-- 2nd law: fmap (f . g) = fmap f . fmap g

-- fmap (f.g) Nothing == Nothing
-- fmap f . fmap g Nothing == fmap f (fmap g Nothing) == fmap f (Nothing) == Nothing
-- fmap (f.g) (Just x) == Just (f.g x)
-- fmap f . fmap g (Just x) == fmap f (Just (g x)) == Just f(g x) == Just (f.g x)

-------------------------------------
-- Ok, moving on to the real work:
-------------------------------------
-- for the [] type constructor
-- fmap :: (a -> b) -> [a] -> [b] 
--instance Functor [] where
--  fmap f a = map f a
--  --> fmap = map

-- 1st law: fmap id = id
-- map id [] == []
-- map id (x:xs) == id x : map id xs == x : (map id xs) == (x:xs)

-- 2nd law: fmap (f . g) = fmap f . fmap g
-- map (f.g) [] == []
-- map f . map g [] == map f [] == []
-- map (f.g) (x:xs) == f(g x) : (map (f.g) xs) == map f (map g (x:xs))
-- map f . map g (x:xs) == f(g x) : (map f . map g xs) == map f (map g (x:xs))
-- not really sure about the proof in the previous 2 lines..


-------------------------------------
-- for `Either a`
data Either a b = Left a | Right b
instance Functor (Either a) where
    fmap f (Left x) = Left x
    fmap f (Right x) = Right (f x)

-- 1st law: fmap id = id
-- fmap id (Left x) == Left x
-- fmap id (Right x) == Right (id x) == Right x

-- 2nd law: fmap (f . g) = fmap f . fmap g
-- fmap (f.g) (Left x) == Left x
-- fmap f . fmap g (Left x) ==  fmap f (Left x) == Left x
-- fmap (f.g) (Right x) == Right (f.g x)
-- fmap f . fmap g (Right x) == fmap f (Right (g x)) == Right (f(g x)) == Right (f.g x)

-------------------------------------------------------------------------------------

data CMaybe a 
  = CNothing 
  | CJust Int a 
  deriving (Show) 

instance Functor CMaybe where
  fmap f CNothing = 
    CNothing
  fmap f (CJust counter x) = 
    CJust (counter+1) (f x)

-- Homework: show that for this instance the functor laws do not hold
-- 1st law fmap id = id
-- fmap id CNothing == CNothing --> OK
-- fmap id (CJust counter x) == CJust (counter+1) (id x)
-- == CJust (counter+1) x != CJust counter x --> NOT OK

-- 2nd law = fmap (f.g) = fmap f . fmap g
-- fmap (f.g) CNothing = CNothing
-- fmap f . fmap g CNothing == fmap f . CNothing == CNothing --> OK
-- fmap (f.g) (CJust counter x) == CJust (counter +1) ((f.g) x)
--      == CJust (counter+1) f(g x)
-- fmap f . fmap g (CJust counter x) == fmap f . (CJust (counter+1) (g x))
--      == CJust ((counter+1)+1) (f(g x)) --> NOT OK


-------------------------------------------------------------------------------------
-- exercise 2 - see AParser.hs
-- https://www.cis.upenn.edu/~cis194/spring13/hw/10-applicative.pdf
-- https://www.cis.upenn.edu/~cis194/spring13/extras/10-applicative/AParser.hs