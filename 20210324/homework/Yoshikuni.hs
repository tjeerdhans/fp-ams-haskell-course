module Yoshikuni where

import GHC.Base hiding (Functor)

class Functor f where
   fmap :: (a -> b) -> f a -> f b 


-- instance Functor (Either a) where
--     fmap _ (Left x) = Left x
--     fmap f (Right x) = Right (f x)


-- Either a, First Law

-- fmap id (Left x) = Left x
-- id Left x = Left x

-- fmap id (Right x) = Right (id x) = Right x
-- id (Right x) = Right x


-- Either a, Second Law

-- fmap (f . g) (Left x) = Left x
-- (fmap f . fmap g) (Left x) = fmap f (fmap g (Left x)) = fmap f (Left x) = Left x

-- fmap (f . g) (Right x) = Right ((f . g) x) = Right (f(g x))
-- (fmap f . fmap g) (Right x) = fmap f (fmap g (Right x)) = fmap f (Right (g x)) = Right (f(g x))



-- instance Functor [] where
--     fmap _ [] = []
--     fmap f (x:xs) = f x : myfmap f xs


-- [], First Law

-- fmap id [] = []
-- id [] = []

-- fmap id [1] = (id 1) : fmap id [] = 1 : [] = [1]
-- id [1] = [1]


-- [], Second Law

-- fmap (f . g) [] = []
-- (fmap f . fmap g) [] = fmap f (fmap g []) = fmap f [] = []

-- fmap (f . g) [x] = (f . g) x : fmap (f . g) [] = f(g x) : [] = [f(g x)]
-- (fmap f . fmap g) [x] = fmap f (fmap g [x]) = fmap f [g x] = [f(g x)]


data CMaybe a 
  = CNothing 
  | CJust Int a 
  deriving (Show) 

instance Functor CMaybe where
  fmap f CNothing = 
    CNothing
  fmap f (CJust counter x) = 
    CJust (counter+1) (f x)


-- First Law

-- fmap id CNothing = CNothing
-- id CNothing = CNothing

-- fmap id (CJust counter x) = CJust (counter+1) (id x) = CJust (counter+1) x
-- id (CJust counter x) = CJust counter x
-- -> First Law is not fulfilled


-- Second Law

-- fmap (f . g) CNothing = CNothing
-- (fmap f . fmap g) CNothing = fmap f (fmap g CNothing) = fmap f (CNothing) = CNothing

-- fmap (f . g) (CJust counter x) = CJust (counter+1) ((f . g) x) = CJust (counter+1) f(g x)
-- (fmap f . fmap g) (CJust counter x) = fmap f (fmap g (CJust counter x)) = fmap f (CJust (counter+1) (g x)) = CJust (counter+2) f(g x)
-- -> Second Law is not fulfilled