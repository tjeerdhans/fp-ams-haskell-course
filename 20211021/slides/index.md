# Haskell: From Beginner to Intermediate #21

## Monad Laws

### FP AMS 21/10/2021

---

![right 100%](learning-curve.png)

# Overall Planning

1. Basics
2. Functors, Applicative Functors, and Monoids
3. **Monads**
4. Lenses, Folds, and Traversals
5. Monad Transformers
6. Prisms and Isos
7. Type-Level Programming

---

![inline](book-of-monads.png)

---

![right](breaking-the-law.jpg)

# Laws?

> "A _law_ is a property of a function, or a set of functions, that relates their behavior in different scenarios."
-- Book of Monads

- compiler can *not* enforce
- form: equality between two expressions ("logical" equality, equivalence)

$$
f \, x \ldots \equiv g \, x \ldots
$$

- laws: functions, functors, and monads

---

# Functions

Function composition:

```haskell
(.) :: (b -> c) -> (a -> b) -> (a -> c)
f . g = \x -> f (g x)
```

Identity function:

```haskell
id :: a -> a
id x = x
```

---

# Function Laws

## Left and Right Identity

```haskell
id . f ≡ f

f . id ≡ f
```

## Associativity

```haskell
f . (g . h) ≡ (f . g) . h
```

--- 

# Proving Laws - Equational Reasoning

```haskell

(f . (g . h)) x

≡ -- by definition of (.) 

f ((g . h) x)

≡ -- by definition of (.) 

f (g (h x))

≡ -- by definition of (.) 

(f . g) (h x)

≡ -- by definition of (.) 

((f . g) . h) x 
```

--- 

# Functor Laws

```haskell
fmap id ≡ id

fmap (f . g) ≡ fmap f . fmap g 
```

---

# Monad Laws

## Left Identity

```haskell
  f x
≡
  do y <-return x
    f y 
```

## Right Identity

```haskell
  m
≡
  do x <-m
     return x 
```
